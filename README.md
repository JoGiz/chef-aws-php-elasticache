# AWS ElastiCache PHP client Chef Cookbook

This cookbook downloads and installs the AWS ElastiCache PHP extension.

Please see the available client versions from the AWS Console.

## Attributes

```ruby
default['aws_php_elasticache']['php_version'] = '5.5'
default['aws_php_elasticache']['arch'] = '64'
```
