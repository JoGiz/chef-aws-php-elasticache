#
# Cookbook Name:: aws-php-elasticache
# Recipe:: default
#

include_recipe "php"

zipfile = "#{Chef::Config[:file_cache_path]}/aws-php-elasticache";
modulepath = '$(pecl7 config-get ext_dir)'
version = node['php']['version']


remote_file zipfile do
    source "https://elasticache-downloads.s3.amazonaws.com/ClusterClient/PHP-#{node['aws_php_elasticache']['php_version']}/latest-#{node['aws_php_elasticache']['arch']}bit"
    action :create
end

execute "Unpack aws elasticache module" do
	command "tar -zxvf #{zipfile} -C #{Chef::Config[:file_cache_path]}"
    action :run
end

bash 'Add php module' do
	cwd "#{Chef::Config[:file_cache_path]}"
  	code <<-EOF
		sofilename=$(find -name "amazon-elasticache-cluster-client.so" | head -1)
		cp "$sofilename" "#{modulepath}/amazon-elasticache-cluster-client.so"
		echo "extension=amazon-elasticache-cluster-client.so" > /etc/php-7.0.d/memcached.ini
  	EOF
  	not_if "which #{node['php']['bin']} --version | grep #{version}"
end

