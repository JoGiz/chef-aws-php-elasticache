sofile = "#{Chef::Config[:file_cache_path]}/amazon-elasticache-cluster-client.so";
modulepath = '$(pecl7 config-get ext_dir)'
version = node['php']['version']

remote_file sofile do
    source "https://s3-eu-west-1.amazonaws.com/egmont-deployment/elasticache/amazon-elasticache-cluster-client.so"
    action :create
end

bash 'Add php module' do
	cwd "#{Chef::Config[:file_cache_path]}"
  	code <<-EOF
		cp "#{sofile}" "#{modulepath}/"
		echo "extension=amazon-elasticache-cluster-client.so" > /etc/php-7.0.d/memcached.ini
  	EOF
  	not_if "which #{node['php']['bin']} --version | grep #{version}"
end
