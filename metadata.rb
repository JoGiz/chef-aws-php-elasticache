name             'aws-php-elasticache'
maintainer       'bedican'
maintainer_email 'hi@bedican.co.uk'
license          'All rights reserved'
description      'Installs the AWS PHP ElastiCache cluster client'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url       'https://bitbucket.org/bedican/chef-aws-php-elasticache' if respond_to?(:source_url)
version          '0.0.1'

depends "php"
