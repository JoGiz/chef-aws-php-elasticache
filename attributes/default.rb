default['aws_php_elasticache']['php_version'] = '5.5'
default['aws_php_elasticache']['arch'] = '64'
default['aws_php_elasticache']['modulepath'] = '/etc/php-7.0.d/'
